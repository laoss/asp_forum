﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chatik.Models
{
    public class Section_Themes
    {
        public Section section;
        public List<Theme> themes;

        public Section_Themes()
        {
            themes = new List<Theme>();
        }
    }
}