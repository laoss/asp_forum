﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chatik.Models
{
    public class AuthData
    {
        public String UserLogin { get; set; }
        public String UserPassword { get; set; }
    }
}