﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Chatik.Models
{
    public class Section
    {
        public int Id { get; set; }
        public int Id_Author { get; set; }
        public String Title { get; set; }
        public  String Description { get; set; }
        public DateTime CreateDT { get; set; }

    }
}