﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chatik.Models
{
    public class User
    {
        public int    Id { get; set; }
        public String Login { get; set; }
        public String PassHash { get; set; }
        public String PassSalt { get; set; }
        public int    ShownData { get; set; }  // Login / RealName / Email
        public String RealName { get; set; }
        public String Status { get; set; }
        public String Email { get; set; }
        public String AvatarFile { get; set; }
        public DateTime Registered { get; set; }
        public DateTime? LastLogin { get; set; }  // Nullable<DateTime>

    }
}