﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chatik.Models
{
    public class Post
    {
        public int Id { get; set; }
        public int Id_Author { get; set; }
        public int Id_Theme { get; set; }
        public int Id_Cite { get; set; }
        public String Message { get; set; }
        public DateTime CreateDT { get; set; }
    }
}