﻿using Antlr.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Net;

namespace Chatik.Models
{
    public class UserUtil
    {
        public static String GetUserShown(int userId)
        {
            var user = GetUser(userId);
            if (user != null)
            {
                switch (user.ShownData)
                {
                    case 1: return user.RealName;
                    case 2: return user.Email;
                    default: return user.Login;
                }
            }
            return null;
        }

        public static User GetUser(int userId)
        {
            return Models.ChatContext.Context.Users.Find(userId);
        }

        public static User GetUser(AuthData authData)
        {
            var user = from u in Models.ChatContext.Context.Users
                       where u.Login == authData.UserLogin
                       select u;
            foreach (var u in user)
            {
                if (
                    Utils.GetSHA256HexString(
                        authData.UserPassword + u.PassSalt)
                    ==
                    u.PassHash)
                {
                    return u;
                }
            }
            return null;
        }

        public static int GetPostCnt(int userId)
        {
            var user = Models.ChatContext.Context.Users.Find(userId);
            if (user == null)
                return 0;
            return (from p in Models.ChatContext.Context.Posts
                    where p.Id_Author == userId
                    select p).Count();
        }

        public static int GetSectionCnt(int userId)
        {
            var user = Models.ChatContext.Context.Users.Find(userId);
            if (user == null)
                return 0;
            return (from s in Models.ChatContext.Context.Sections
                    where s.Id_Author == userId
                    select s).Count();
        }

        public static int GetThemeCnt(int userId)
        {
            var user = Models.ChatContext.Context.Users.Find(userId);
            if (user == null)
                return 0;
            return (from t in Models.ChatContext.Context.Themes
                    where t.Id_Author == userId
                    select t).Count();
        }

        public static Post GetLastUserPost(int userId)
        {
            /*var user = Models.ChatContext.Context.Users.Find(userId);
            if (user == null)
                return null;*/
            var query = from p in Models.ChatContext.Context.Posts
                        where p.Id_Author == userId
                        orderby p.CreateDT descending
                        select p;
            if (query.Count() > 0) return query.First();
            else return null;
        }

        public static int GetCites(int userId)
        {
            return (from p in Models.ChatContext.Context.Posts
                    where (p.Id_Cite != 0) && (p.Id_Author == userId)
                    select p).Count();
        }

        static public bool IsBanned(int userId)
        {
            var user = Models.ChatContext.Context.Users.Find(userId);
            if (user == null)
                return true;
            return IsBanned(user);
                
        }

        static public bool IsBanned(User user)
        {
            return
                user.LastLogin.GetValueOrDefault()
                .Equals(
                    DateTime.Parse(
                        System
                        .Configuration
                        .ConfigurationManager
                        .AppSettings["banDate"]));
        }

        static public bool SendMail(int senderId, int citeId, String message)
        {
            MailAddress from = new MailAddress("testunproverialovich@gmail.com", "ASP Chatik");

            var post = (from p in Models.ChatContext.Context.Posts
                         where p.Id == citeId
                         orderby p.CreateDT descending
                         select p).First();            
            var userTo = (from u in Models.ChatContext.Context.Users
                        where u.Id == post.Id_Author
                        orderby u.Registered descending
                        select u).First();

            MailAddress to = new MailAddress(userTo.Email);
            MailMessage msg = new MailMessage(from, to);
            msg.Subject = "Cite notification";
            msg.Body = "Здравствуйте, "
                + userTo.RealName
                +"! \n Вы получили ответ на сообщение от  "
                + GetUserShown(senderId)
                + ". \n\nСообщение: \n" + message;
            msg.IsBodyHtml = false;

            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 25);
            smtp.Credentials = new NetworkCredential("testunproverialovich@gmail.com", "Ntcnbhjdfybt");
            smtp.EnableSsl = true;
            smtp.Send(msg);

            return true;
        }
    }
}