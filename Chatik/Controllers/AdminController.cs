﻿using Chatik.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Chatik.Controllers
{
    public class AdminController : Controller
    {
        private Models.ChatContext chat;
        // GET: /Admin - portal
        public ActionResult Index()
        {
            return View();
        }

        // GET: admin/users
        public ViewResult Users()
        {
            chat = Models.ChatContext.Context ?? new Models.ChatContext();

            List<Models.UserAdminData> userAdminData = new List<Models.UserAdminData>();
            foreach(var user in Models.ChatContext.Context.Users.ToList())
            {
                userAdminData.Add(new Models.UserAdminData()
                {
                    user = user,
                    isBan = (user.LastLogin.GetValueOrDefault().Equals(DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings["banDate"]))),
                    shown = user.ShownData == 0 ? "Логин" :
                            user.ShownData == 1 ? "Имя" : "Почта",
                    postCnt = Models.UserUtil.GetPostCnt(user.Id),
                    themeCnt = Models.UserUtil.GetThemeCnt(user.Id),
                    secCnt = Models.UserUtil.GetSectionCnt(user.Id),
                    lastPost = Models.UserUtil.GetLastUserPost(user.Id),
                    cites = Models.UserUtil.GetCites(user.Id)
                });
            }
            ViewBag.userAdminData = userAdminData;
            return View();
            /*
            && t.CreateDT.Day.Equals(DateTime.Now.Day) && t.CreateDT.Month.Equals(DateTime.Now.Month)
                      && t.CreateDT.Year.Equals(DateTime.Now.Year)*/
        }

        public JsonResult ActivateUser(int status, int uid)
        {
            
            Models.User user = Models.UserUtil.GetUser(uid);
            int result = 0;

            if (user == null)
            {
                result = -1;
            } 
            else
            {
                if (status == 1)
                {
                    user.LastLogin = DateTime.Now;
                }
                else if (status == -1)
                {
                    user.LastLogin = DateTime.Parse(
                        System
                        .Configuration
                        .ConfigurationManager
                        .AppSettings["banDate"]
                        );
                }
                else
                    result = -2;

                if(result == 0)
                {
                    try
                    {
                        Models.ChatContext.Context.SaveChanges();
                        result = 1;
                    }
                    catch { result = -3; }
                }
            }
            
            return Json(new
            {
                status,
                uid,
                result
            },
            JsonRequestBehavior.AllowGet);
        }
    }
}