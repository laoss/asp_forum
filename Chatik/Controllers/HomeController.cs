﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Chatik.Models;
using System.Web.Services.Protocols;

namespace Chatik.Controllers
{
    public class HomeController : Controller
    {
        Models.ChatContext chat;

        public ActionResult Index()
        {
            if(chat == null) chat = new Models.ChatContext();

            int n = chat.Sections.Count();
            if (n == 0)  // No sections - create
            {
                chat.Sections.Add(
                    new Models.Section()
                    {
                        Id = 1,
                        Id_Author = 1,
                        Title = "Продам / куплю",
                        Description = "Раздел о барахолке",
                        CreateDT = DateTime.Now
                    }
                );
                chat.Sections.Add(
                    new Models.Section()
                    {
                        Id = 2,
                        Id_Author = 2,
                        Title = "Болталка",
                        Description = "Разговоры на произвольные темы",
                        CreateDT = DateTime.Now
                    }
                );
                chat.SaveChanges();
            }

            n = chat.Themes.Count();
            if (n == 0)
            {
                chat.Themes.Add(
                    new Models.Theme()
                    {
                        Id = 1,
                        Id_Author = 1,
                        Id_Section = 1,
                        Title = "Продукты питания",
                        Description = "Покупка / продажа продуктов питания (Б/У)",
                        CreateDT = DateTime.Now
                    }
                );
                chat.Themes.Add(
                    new Models.Theme()
                    {
                        Id = 2,
                        Id_Author = 2,
                        Id_Section = 1,
                        Title = "Автозапчасти",
                        Description = "Покупка / продажа поломанных деталей от мотоциклов и автомобилей",
                        CreateDT = DateTime.Now
                    }
                );
                chat.Themes.Add(
                    new Models.Theme()
                    {
                        Id = 3,
                        Id_Author = 1,
                        Id_Section = 2,
                        Title = "Знакомства",
                        Description = "Раздел для знакомств и общения",
                        CreateDT = DateTime.Now
                    }
                );

                chat.Themes.Add(
                    new Models.Theme()
                    {
                        Id = 4,
                        Id_Author = 2,
                        Id_Section = 2,
                        Title = "Предсказания",
                        Description = "Прогнозы погоды, политики, экономики",
                        CreateDT = DateTime.Now
                    }
                );
                chat.SaveChanges();
            }
            //ViewBag.UsersCount = chat.Users.Count();
            //ViewBag.SectionsCount = chat.Sections.Count();
            //ViewBag.ThemesCount = chat.Themes.Count();
            //ViewBag.PostsCount = chat.Posts.Count();

            ViewBag.Sections = chat.Sections.ToList();
            ViewBag.Themes = chat.Themes.ToList();

            var themes = from t in chat.Themes
                         join s in chat.Sections on t.Id_Section equals s.Id
                         select new Models.TwoStrings()
                         {
                             s1 = t.Title,
                             s2 = s.Title
                         };

            List<Models.Section_Themes> st = new List<Models.Section_Themes>();
            
            foreach( var s in chat.Sections)
            {
                st.Add(new Models.Section_Themes()
                {
                    section = s
                });
            }

            foreach(var s in st)
            {
                var q = from t in chat.Themes
                        where t.Id_Section == s.section.Id
                        select t;

                foreach(Models.Theme t in q)
                {
                    s.themes.Add(t);
                }
            }
            /*foreach(var ts in themes)
            {
                ViewBag.Section_Themes = ts.Section_Title + ts.Themes_Title;
            }*/
            //ViewBag.Section_Themes = themes;

            ViewBag.Section_Themes = st;
            return View();
        }

        public ActionResult About(int id)
        {
            if (chat == null) chat = new Models.ChatContext();

            var posts = from   p in chat.Posts
                        where p.Id_Theme == id
                        select p;
            ViewBag.Posts = posts.ToList();
            ViewBag.Id_Theme = id;
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Registration()
        {
            ViewBag.Message = "Registration page.";

            return View();
        }

        public ActionResult RegUser(Models.User user)
        {
            if (!String.IsNullOrEmpty(user.Login))
            {
                var sb = new System.Text.StringBuilder();
                var rnd = new Random();

                for (int i = 0; i < 32; i++)
                {
                    sb.Append(((byte)rnd.Next()).ToString("X2"));
                }
                user.PassSalt = sb.ToString();
                user.PassHash = Utils.GetSHA256HexString(user.PassHash + user.PassSalt);
                user.Registered = DateTime.Now;

                if(Request.Files.Count > 0)
                {
                    var file = (HttpPostedFileWrapper)Request.Files[0];
                    string path = Server.MapPath("/");
                    //string path = Directory.GetCurrentDirectory();  // "C:\\ProgramFiles (x86)\\IIS Express
                    //string path = System.Environment.CurrentDirectory;  // ... IIS Express
                    //string path = Server.MapPath("/");
                    //string path = System.Domain.CurrentDomain.BaseDirectory;  // ...ASP_CHAT
                    string AvatarStorage =
                        System
                        .Configuration
                        .ConfigurationManager
                        .AppSettings["AvatarStorage"];
                    string fname = 
                        path
                        + @"\"
                        + AvatarStorage
                        + @"\"
                        + file.FileName;
                    using (var sw = new FileStream(fname, FileMode.Create))
                    {
                        file.InputStream.CopyTo(sw);
                    }
                    user.AvatarFile = file.FileName;
                }

                Models.ChatContext chat = new Models.ChatContext();
                chat.Users.Add(user);
                chat.SaveChanges();
            }            
            ViewBag.User = user;

            return View();            
        }


    }
}