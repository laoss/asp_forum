﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chatik
{
    public static class Utils
    {
        public static string GetMD5HexString(string str)
        {
            using (var hasher = System.Security.Cryptography.MD5.Create())
            {
                byte[] strBytes = System.Text.Encoding.ASCII.GetBytes(str);
                byte[] hashBytes = hasher.ComputeHash(strBytes);

                var sb = new System.Text.StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public static string GetSHA256HexString(string str)
        {
            using (var hasher = System.Security.Cryptography.SHA256.Create())
            {
                byte[] strBytes = System.Text.Encoding.ASCII.GetBytes(str);
                byte[] hashBytes = hasher.ComputeHash(strBytes);

                var sb = new System.Text.StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

    }
}